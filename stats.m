clear all
clf

% Input
network_size = 1000000
min_sec_size = 100
num_elders_per_sec = 10
split_buffer = 20

% This seems pretty arbirary at this point, maybe there is a better choice?
sec_size_dist = @() 2^rand() - 1;
%sec_size_dist = @() 2^(rand()^2) - 1;

[connections, section_size] = section_connections(
    network_size,
    min_sec_size,
    num_elders_per_sec,
    split_buffer,
    sec_size_dist
);

% Visualise section size distribution
hist(section_size, 50)
xlabel('Section size')
ylabel('Number of sections')
title('Section size distribution')

% Output
num_connections_min = min(connections)
num_connections_max = max(connections)
num_connections_median = median(connections)

