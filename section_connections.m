function [connections, section_size] = section_connections(...
        network_size,
        min_sec_size,
        num_elders_per_sec,
        split_buffer,
        sec_size_dist
    )

    % 1. Randomly generate sections
    max_iter = 100000;
    for ii = 1:max_iter
        section_size(ii) = round((min_sec_size + 2*split_buffer) * sec_size_dist() + min_sec_size);
        if sum(section_size) > network_size
            break
        end
    end
    assert(ii < max_iter);

    prefix_length = ceil(log2(length(section_size)));
    num_neighbour_sections = prefix_length;

    % 2. Count number of connections per section
    for ii = 1:length(section_size)
        adults(ii) = section_size(ii) - num_elders_per_sec;

        % All nodes in the current section
        connections(ii) = section_size(ii);

        % All nodes in sibling section
        I = randperm(length(section_size))(1);
        sibling_section_size = section_size(I);
        connections(ii) += sibling_section_size;

        % Elders in neighbour sections
        connections(ii) += num_neighbour_sections * num_elders_per_sec;
    end
end
